# Graphical interface for my music database #

My music database is comprised of musical artists, groups, and albums. I built this GUI to keep track of the music I listen to. My own personal allmusic.com.

Built in Angular 1.4 and Rails. I have stopped development on this project since I have decided to recreate it in Angular 2 in a new repo.