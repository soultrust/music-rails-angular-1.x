(function() {
  'use strict';

  angular.module('app', ['ui.router', 'smart-table'])
    .config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
    function($stateProvider, $urlRouterProvider, $locationProvider) {
      $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
      });

      $stateProvider
        .state('artist-new', {
          url: '/artists/new',
          templateUrl: '/templates/artists/create-edit.html',
          controller: 'ArtistNewCtrl',
          controllerAs: 'artistEdit'
        })
        .state('artist-edit', {
          url: '/artists/:id/edit',
          templateUrl: '/templates/artists/create-edit.html',
          controller: 'ArtistEditCtrl',
          controllerAs: 'artistEdit'
        })
        .state('artist-show', {
          url: '/artists/:id',
          templateUrl: '/templates/artists/show.html',
          controller: 'ArtistShowCtrl',
          controllerAs: 'artistShow'
        })
        .state('artist-list', {
          url: '/artists',
          templateUrl: '/templates/artists/index.html',
          controller: 'ArtistListCtrl',
          controllerAs: 'artistList'
        })

        .state('album-new', {
          url: '/albums/new',
          templateUrl: '/templates/albums/create-edit.html',
          controller: 'AlbumNewCtrl'
        })
        .state('album-edit', {
          url: '/albums/:id/edit',
          templateUrl: '/templates/albums/create-edit.html',
          controller: 'AlbumEditCtrl'
        })
        .state('album-show', {
          url: '/albums/:id',
          templateUrl: '/templates/albums/show.html',
          controller: 'AlbumShowCtrl'
        })
        .state('album-list', {
          url: '/albums',
          templateUrl: '/templates/albums/index.html',
          controller: 'AlbumListCtrl',
          controllerAs: 'albumListCtrl'
        })

        .state('group-new', {
          url: '/groups/new',
          templateUrl: '/templates/groups/create-edit.html',
          controller: 'GroupNewCtrl'
        })
        .state('group-edit', {
          url: '/groups/:id/edit',
          templateUrl: '/templates/groups/create-edit.html',
          controller: 'GroupEditCtrl'
        })
        .state('group-show', {
          url: '/groups/:id',
          templateUrl: '/templates/groups/show.html',
          controller: 'GroupShowCtrl'
        })
        .state('group-list', {
          url: '/groups',
          templateUrl: '/templates/groups/index.html',
          controller: 'GroupListCtrl',
          controllerAs: 'groupListCtrl'
        })

        $urlRouterProvider.otherwise('/artists');

    }])
    .controller('ArtistNewCtrl', ['$scope', '$http', '$stateParams', '$state',
      function($scope, $http, $stateParams, $state) {

        var vm = this;
        vm.artist = {};

        vm.submit = function() {
          $http.post('/api/v1/artists', vm.artist).then(
            function() {
              $state.go('artist-list');
            }
          )
        };

      }
    ])
    .controller('ArtistEditCtrl', ['$scope', '$http', '$stateParams', '$state',
      function($scope, $http, $stateParams, $state) {

        var vm = this;

        $http.get('/api/v1/artists/'+ $stateParams.id).then(function(resp) {
          vm.artist = resp.data;
        });

        vm.submit = function() {
          $http.put('/api/v1/artists/'+ $stateParams.id, vm.artist).then(
            function() {
              $state.go('artist-list');
            }
          )
        };

      }
    ])
    .controller('ArtistShowCtrl', ['$scope', '$http', '$stateParams',
      function($scope, $http, $stateParams) {

        var vm = this;

        $http.get('/api/v1/artists/'+ $stateParams.id).then(function(resp) {
          vm.artist = resp.data;
        });

      }
    ])
    .controller('ArtistListCtrl', ['$scope', '$http',
      function($scope, $http) {

        var vm = this;

        $http.get('/api/v1/artists').then(function(resp) {
            vm.artists = resp.data;
        });

        vm.remove = function(artist) {

          if (confirm('Are you sure you want to delete '+ artist.first_name +' '+ artist.last_name +'?')) {
            $http.delete('/api/v1/artists/'+ artist.id).then(
              function() {
                vm.artists.find(function(a, i) {
                  if (a === artist) {
                    vm.artists.splice(i, 1);
                  }
                });
              }
            );
          }

        };

      }
    ])

    .controller('AlbumNewCtrl', ['$scope', '$http', '$stateParams', '$state',
      function($scope, $http, $stateParams, $state) {

        $scope.album = {};

        $scope.submit = function() {
          $http.post('/api/v1/albums', $scope.album).then(
            function() {
              $state.go('album-list');
            }
          )
        };

      }
    ])
    .controller('AlbumEditCtrl', ['$scope', '$http', '$stateParams', '$state',
      function($scope, $http, $stateParams, $state) {

        $http.get('/api/v1/albums/'+ $stateParams.id).then(function(resp) {
          $scope.album = resp.data;
        });

        $scope.submit = function() {
          $http.put('/api/v1/albums/'+ $stateParams.id, $scope.album).then(
            function() {
              $state.go('album-list');
            }
          )
        };

      }
    ])
    .controller('AlbumShowCtrl', ['$scope', '$http', '$stateParams',
      function($scope, $http, $stateParams) {

        $http.get('/api/v1/albums/'+ $stateParams.id).then(function(resp) {
          $scope.album = resp.data;
        });

      }
    ])
    .controller('AlbumListCtrl', ['$scope', '$http',
      function($scope, $http) {

        var listCtrl = this;

        $http.get('/api/v1/albums').then(function(resp) {
            listCtrl.albums = resp.data;
        });

        $scope.remove = function(artist) {

          if (confirm('Are you sure you want to delete '+ artist.name +'?')) {
            $http.delete('/api/v1/albums/'+ album.id).then(
              function() {
                listCtrl.albums.find(function(p, i) {
                  if (p === place) {
                    listCtrl.places.splice(i, 1);
                  }
                });
              }
            );
          }

        };

      }
    ])

    .controller('GroupNewCtrl', ['$scope', '$http', '$stateParams', '$state',
      function($scope, $http, $stateParams, $state) {

        $scope.group = {};

        $scope.submit = function() {
          $http.post('/api/v1/groups', $scope.group).then(
            function() {
              $state.go('group-list');
            }
          )
        };

      }
    ])
    .controller('GroupEditCtrl', ['$scope', '$http', '$stateParams', '$state',
      function($scope, $http, $stateParams, $state) {

        $http.get('/api/v1/groups/'+ $stateParams.id).then(function(resp) {
          $scope.group = resp.data;
        });

        $scope.submit = function() {
          $http.put('/api/v1/groups/'+ $stateParams.id, $scope.group).then(
            function() {
              $state.go('group-list');
            }
          )
        };

      }
    ])
    .controller('GroupShowCtrl', ['$scope', '$http', '$stateParams',
      function($scope, $http, $stateParams) {

        $http.get('/api/v1/groups/'+ $stateParams.id).then(function(resp) {
          $scope.group = resp.data;
        });

      }
    ])
    .controller('GroupListCtrl', ['$scope', '$http',
      function($scope, $http) {

        var vm = this;

        $http.get('/api/v1/groups').then(function(resp) {
            $scope.groups = resp.data;
        });

        $scope.remove = function(artist) {

          if (confirm('Are you sure you want to delete '+ artist.name +'?')) {
            $http.delete('/api/v1/groups/'+ group.id).then(
              function() {
                $scope.albums.find(function(g, i) {
                  if (g === group) {
                    $scope.groups.splice(i, 1);
                  }
                });
              }
            );
          }

        };

      }
    ]);

})();
