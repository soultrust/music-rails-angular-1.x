class Album < ActiveRecord::Base
  enum release_date_accuracy: {
    YEAR: 0,
    MONTH: 1,
    DAY: 2
  }
end
